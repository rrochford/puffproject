﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetLives : MonoBehaviour {

    public UIValue livesValue;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        livesValue.SetCurValue(GameManager.instance.SpawnedPlayer.GetComponent<Player>().CurLives);
	}
}
