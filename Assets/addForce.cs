﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class addForce : MonoBehaviour
{
    public Rigidbody2D rb2d;
    public float thrust;

    private void Start()
    {
        rb2d = gameObject.GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        rb2d.AddForce(transform.up * thrust);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        
    }
}

    
