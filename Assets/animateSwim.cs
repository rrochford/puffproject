﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animateSwim : MonoBehaviour
{

    Animator puffAnimator;
    float updateSpeed;


	void Start ()
    {
        GameObject puffSkin = GameObject.Find("FishSkin");
        puffAnimator = puffSkin.GetComponent<Animator>();       
	}
	
	void Update ()
    {
        updateSpeed = GameObject.Find("PuffController").GetComponent<FishController>().VelocitySpeed;
        puffAnimator.SetFloat("Speed", updateSpeed);
	}
}
