﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Unit
{
    [SerializeField] private bool setData;

    private int curPoints;
    public int CurPoints { get { return curPoints; } }

    public new PlayerSkinData CurUnitData { get { return (PlayerSkinData)curData; } }
    public new UIPlayer UI { get { return (UIPlayer)ui; } }
    private UserDataManager dataManager;
    private SpawnCheckPointManager spawnManager;
    private PlayerSkinManager skinManager;

    private PlayerSoundFX soundFX;
    private User curUser;

    protected override void GetComponents()
    {
        base.GetComponents();
        //get components
        var gm = GameManager.instance;
        dataManager = gm.GetUserDataManager();
        spawnManager = gm.GetSpawnManager();
        skinManager = gm.GetSkinManager();
        gm.SpawnedPlayer = this;
    }

    protected override void SetFirstData()
    {
        if (setData)
        {
            curData = (PlayerSkinData)data;
            SetData(curData);
        }
        else
        {
            curUser = dataManager.GetCurUser();
            curData = skinManager.playerSkins[curUser.playerSkinInd];
            SetData(curData);
            curLives = curUser.lives;
        }

        //get sound
        soundFX = curSkin.GetComponent<PlayerSoundFX>();
    }


    public override void SetData(UnitData _skinData, bool _resetLives = true)
    {
        base.SetData(_skinData);

        //update data
        if (skinManager.playerSkins.IndexOf((PlayerSkinData)curData) != -1)
            dataManager.SetPlayerSkinData(skinManager.playerSkins.IndexOf((PlayerSkinData)curData));
        else
            Debug.Log("Make sure you add " + _skinData + " to the skin manager: " + skinManager + "!");

    }

    public void AddPoints(int _amount)
    {
        curPoints += _amount;
        if (UI)
            UI.SetPointsValue(curPoints);
    }

    public override void RefreshUIValues()
    {
        base.RefreshUIValues();
        if (UI)
        {
            UI.SetLivesValue(curLives);
            UI.SetPointsValue(curPoints);
        }
    }

    public override void Die(string _reason)
    {
        base.Die();

        //play death sound
        if (soundFX)
            soundFX.PlayDeathSound();
    }

    protected override void ResetUnitPosition(Vector2 _pos)
    {
        var pos = spawnManager.GetCurCheckPointPos();
        if (pos != default(Vector2))
            base.ResetUnitPosition(pos);
        else
            Debug.LogError("Failed to respawn player!");
    }

    public void AddLives(int _amount)
    {
        curLives += _amount;
    }

}
