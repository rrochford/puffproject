﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BoolWrapper
{
    public bool Value { get; set; }
    public BoolWrapper(bool value) { this.Value = value; }
}

[System.Serializable]
public class FloatWrapper
{
    public float Value { get; set; }
    public FloatWrapper(float value) { this.Value = value; }
}

[System.Serializable]
public class InputProperty
{
    public string stringValue;
    public int indexValue;
}

[System.Serializable]
public class TagProperty
{
    public string stringValue;
}

[System.Serializable]
public class LayerProperty
{
    public int indexValue;
    public string stringValue;
    public LayerMask maskValue;
}

[System.Serializable]
public class ChildName
{
    public bool overrideParent;
    public string overridePropertyName;
    public GameObject parent;
    public string stringValue;
    public int indexValue;
    public Transform transformValue { get { return parent.transform.FindDeepChild(stringValue); } }
}

[System.Serializable]
public class AnimatorStateProperty
{
    public string stateToPlay;
    public float crossfadeTime;
    public string exitState;
}

[System.Serializable]
public class AnimatorParamStateInfo
{
    public int indexValue;
    public string stringValue;
    public int layer;
}

[System.Serializable]
public class IndexStringProperty
{
    public int indexValue;
    public string stringValue;
}



