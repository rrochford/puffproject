﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIUnit : MonoBehaviour
{
    [SerializeField] protected UIValue hp;

    public virtual void SetHPValue(float _value)
    {
        hp.SetCurValue(_value);
    }
}
