﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(PlayerEquip))]
public class PlayerEquipEditor : UnitEquipEditor
{

    private SerializedProperty quickMenuButtons;
    private SerializedProperty seperateUseButtonsForItems;
    private SerializedProperty useButtons;
    private SerializedProperty useButton;
    private SerializedProperty enableToggleSwitch;
    private SerializedProperty toggleForwardsButton;
    private SerializedProperty toggleBackwardsButton;
    private SerializedProperty equipButton;
    private SerializedProperty dropButton;


    public override void GetProperties()
    {
        base.GetProperties();
        quickMenuButtons = sourceRef.FindProperty("quickMenuButtons");
        seperateUseButtonsForItems = sourceRef.FindProperty("seperateUseButtonsForItems");
        useButtons = sourceRef.FindProperty("useButtons");
        useButton = sourceRef.FindProperty("useButton");
        enableToggleSwitch = sourceRef.FindProperty("enableToggleSwitch");
        toggleForwardsButton = sourceRef.FindProperty("toggleForwardsButton");
        toggleBackwardsButton = sourceRef.FindProperty("toggleBackwardsButton");
        equipButton = sourceRef.FindProperty("equipButton");
        dropButton = sourceRef.FindProperty("dropButton");
    }

    public override void SetProperties()
    {
        base.SetProperties();
        EditorGUILayout.PropertyField(seperateUseButtonsForItems);
        if (seperateUseButtonsForItems.boolValue)
        {
            useButtons.arraySize = itemDatas.arraySize;
            EditorGUILayout.PropertyField(useButtons, true);
        }
        else
        {
            if (setInventoryItems.boolValue)
                quickMenuButtons.arraySize = itemDatas.arraySize;
            else
                quickMenuButtons.arraySize = maxItems.intValue;
            if (!setAllItemsActive.boolValue)
                EditorGUILayout.PropertyField(quickMenuButtons, true);

            EditorGUILayout.PropertyField(enableToggleSwitch);
            if (enableToggleSwitch.boolValue)
            {
                EditorGUI.indentLevel++;
                EditorGUILayout.PropertyField(toggleForwardsButton);
                EditorGUILayout.PropertyField(toggleBackwardsButton);
                EditorGUI.indentLevel--;
            }


            EditorGUILayout.PropertyField(useButton);
            if (!autoEquipItems.boolValue)
                EditorGUILayout.PropertyField(equipButton);
            EditorGUILayout.PropertyField(dropButton);
        }


    }

    

}
