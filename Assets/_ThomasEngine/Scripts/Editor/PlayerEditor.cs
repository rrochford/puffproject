﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(Player))]
public class PlayerEditor : UnitEditor
{

    //health
    private SerializedProperty setData;

    public override void GetProperties()
    {
        base.GetProperties();
        setData = sourceRef.FindProperty("setData");
    }

    protected override void DisplayDataProperties()
    {
        EditorGUILayout.PropertyField(setData);
        if (setData.boolValue)
        {
            base.DisplayDataProperties();
        }
        
    }

}
