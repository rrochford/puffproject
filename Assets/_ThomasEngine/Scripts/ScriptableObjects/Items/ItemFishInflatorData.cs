﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FishInflator", menuName = "Data/Items/Controller/FishInflator", order = 1)]
public class ItemFishInflatorData : ItemUseableData
{
    public float amountDrainSpeed = 1;
    public bool drainOnHPHit;
}
