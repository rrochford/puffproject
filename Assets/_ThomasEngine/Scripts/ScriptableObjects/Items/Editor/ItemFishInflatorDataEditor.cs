﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ItemFishInflatorData))]
public class ItemFishInflatorDataEditor : ItemUseableDataEditor
{
    public SerializedProperty amountDrainSpeed;

    public override void OnEnable()
    {
        base.OnEnable();
        source = (ItemFishInflatorData)target;
        source.linkedType = typeof(ItemFishInflator);
    }

    public override void GetProperties()
    {
        base.GetProperties();
        amountDrainSpeed = sourceRef.FindProperty("amountDrainSpeed");
    }

    public override void SetProperties()
    {
        base.SetProperties();
        EditorGUILayout.LabelField("Fish Inflator Properties",boldStyle);
        EditorGUILayout.PropertyField(amountDrainSpeed);

    }

}
