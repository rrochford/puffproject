﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using System;

[CustomEditor(typeof(ProjectileData))]
public class ProjectileDataEditor : LinkableDataEditor
{
    protected SerializedProperty detectType;
    protected SerializedProperty lifeTime;
    protected SerializedProperty detectOffset;
    protected SerializedProperty detectRadius;
    protected SerializedProperty detectSize;
    protected SerializedProperty hitMaxAmount;
    protected SerializedProperty spawnOnImpact;
    protected SerializedProperty interactFX;

    public override void OnEnable()
    {
        base.OnEnable();
        source = (ProjectileData)target;
        source.linkedType = typeof(Projectile);
    }

    public override void GetProperties()
    {
        base.GetProperties();
        detectType = sourceRef.FindProperty("detectType");
        lifeTime = sourceRef.FindProperty("lifeTime");
        detectOffset = sourceRef.FindProperty("detectOffset");
        detectRadius = sourceRef.FindProperty("detectRadius");
        detectSize = sourceRef.FindProperty("detectSize");
        hitMaxAmount = sourceRef.FindProperty("hitMaxAmount");
        spawnOnImpact = sourceRef.FindProperty("spawnOnImpact");
        interactFX = sourceRef.FindProperty("interactFX");
    }

    public override void SetProperties()
    {
        base.SetProperties();
        EditorGUILayout.PropertyField(detectType);
        if (detectType.enumValueIndex != 2)
        {
            EditorGUILayout.PropertyField(detectOffset);
            if (detectType.enumValueIndex == 0)
                EditorGUILayout.PropertyField(detectRadius);
            else
                EditorGUILayout.PropertyField(detectSize);
        }
        EditorGUILayout.PropertyField(hitMaxAmount);
        EditorGUILayout.PropertyField(lifeTime);
        EditorGUILayout.PropertyField(spawnOnImpact, true);
        EditorGUILayout.PropertyField(interactFX, true);

    }

}
