﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(UserDataManager))]
public class UserDataManagerEditor : Editor
{

    protected SerializedObject sourceRef;
    protected UserDataManager source;

    protected SerializedProperty maxUsers;
    protected SerializedProperty users;


    private void OnEnable()
    {
        source = (UserDataManager)target;
        sourceRef = serializedObject;

        source.InitializeData();

        GetProperties();
    }

    public override void OnInspectorGUI()
    {
        SetProperties();
        sourceRef.ApplyModifiedProperties();
    }

    private void GetProperties()
    {
        maxUsers = sourceRef.FindProperty("maxUsers");
        users = sourceRef.FindProperty("users");
    }

    private void SetProperties()
    {
        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(maxUsers);
        DisplayNewUserFields();
        DisplayAllUsers();

    }

    private void DisplayNewUserFields()
    {
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("New User:");
        source.UserNameInput = EditorGUILayout.TextField(source.UserNameInput);
        if (GUILayout.Button("Create User"))
        {
            source.CreateUser(users.arraySize);
            sourceRef.Update();
        }
        EditorGUILayout.Space();
    }

    void DisplayAllUsers()
    {
        for (int i = 0; i < users.arraySize; i++)
        {
            if (i == 0)
                EditorGUILayout.LabelField("-------------------------");
            var user = users.GetArrayElementAtIndex(i);
            var playerName = user.FindPropertyRelative("playerName");
            //var playerSkinInd = user.FindPropertyRelative("playerSkinInd");
            //var userId = user.FindPropertyRelative("userId");
            //var saveSlotId = user.FindPropertyRelative("saveSlotId");
            var maxHealth = user.FindPropertyRelative("maxHealth");
            var lives = user.FindPropertyRelative("lives");
            var points = user.FindPropertyRelative("points");
            //var levelUnlocked = user.FindPropertyRelative("levelUnlocked");
            var levelName = user.FindPropertyRelative("levelName");
            //var curCheckPoint = user.FindPropertyRelative("curCheckPoint");
            //var inventoryItems = user.FindPropertyRelative("inventoryItems");

            EditorGUILayout.LabelField("User Name: ", playerName.stringValue);
            EditorGUILayout.LabelField("Max Health: ", maxHealth.intValue.ToString());
            EditorGUILayout.LabelField("Lives: ", lives.intValue.ToString());
            EditorGUILayout.LabelField("Points: ", points.intValue.ToString());
            EditorGUILayout.LabelField("Level Unlocked: ", levelName.stringValue);

            if (GUILayout.Button("Delete User"))
            {
                source.RemoveUser(i);
                sourceRef.Update();
            }
            EditorGUILayout.LabelField("-------------------------");
        }
    }

}
