﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

//This is a custom serilizable detect zone property using Overlap functions.
//There is a custom editor property drawer for this class
[System.Serializable]
public class DetectZone
{
    public string zoneName;
    public enum PositionType { None, Local, World }
    public enum DetectAreaType { Circle, Box }
    public LayerMask detectMask;
    public DetectAreaType detectType;
    public PositionType positionType;
    public Transform trans;
    public Vector2 worldPos;
    public Vector2 offset;
    public Vector2 size = Vector2.one;
    public bool useTransformZAngle;
    public float angle = 0;
    public float radius = 1;
    public Color debugColor = Color.cyan;

    //gui stuff
    public Vector2 handlePoint;

    private Vector2 curDetectPos;

    void SetCurDetectPos(Transform _trans = null)
    {
        if (positionType == PositionType.None && _trans)
            curDetectPos = _trans.TransformPoint(offset);
        else if (positionType == PositionType.Local && trans)
            curDetectPos = trans.TransformPoint(offset);
        else
            curDetectPos = worldPos + offset;

        if (useTransformZAngle)
        {
            if (positionType == PositionType.Local && trans)
                angle = trans.eulerAngles.z;
            else if (positionType == PositionType.None && _trans)
                angle = _trans.eulerAngles.z;
        }
    }

    public Collider2D[] DetectCollidersNonAlloc(Transform _trans = null, int _maxAmount = 1)
    {
        SetCurDetectPos(_trans);
        Collider2D[] cols = new Collider2D[_maxAmount];
        if (detectType == DetectAreaType.Circle)
            Physics2D.OverlapCircleNonAlloc(curDetectPos, radius, cols, detectMask);
        else if (detectType == DetectAreaType.Box)
            Physics2D.OverlapBoxNonAlloc(curDetectPos, size, angle, cols, detectMask);

        return cols;
    }

    public Collider2D[] DetectColliders(Transform _trans = null)
    {
        SetCurDetectPos(_trans);
        if (detectType == DetectAreaType.Circle)
            return Physics2D.OverlapCircleAll(curDetectPos, radius, detectMask);
        else if (detectType == DetectAreaType.Box)
            return Physics2D.OverlapBoxAll(curDetectPos, size, angle, detectMask);
        return null;
    }

    public Collider2D DetectCollider(Transform _trans = null)
    {
        SetCurDetectPos(_trans);
        return DetectCollider(curDetectPos);
    }

    public Collider2D DetectCollider(Vector2 _pos)
    {
        if (detectType == DetectAreaType.Circle)
            return Physics2D.OverlapCircle(_pos, radius, detectMask);
        else if (detectType == DetectAreaType.Box)
            return Physics2D.OverlapBox(_pos, size, angle, detectMask);

        return null;
    }

    public bool Detected(Transform _trans = null)
    {
        return DetectCollider(_trans);
    }

#if UNITY_EDITOR
    public void DrawDetectZone(Object _source, SerializedObject _sourceRef, Transform _sourceTrans = null)
    {
        Handles.color = debugColor;
        EditorGUI.BeginChangeCheck();

        var pos = offset;
        if (positionType == PositionType.World)
        {
            handlePoint = Handles.PositionHandle(handlePoint, Quaternion.identity);

            //position points after dragging
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(_source, "Modified " + _source + " properties.");
                worldPos = handlePoint;
                _sourceRef.ApplyModifiedProperties();
                _sourceRef.Update();
            }

            //get final position
            pos = worldPos + offset;
        }
        else if (positionType == PositionType.Local && trans)
                pos = trans.TransformPoint(offset);
        else if (positionType == PositionType.None && _sourceTrans)
                pos = _sourceTrans.TransformPoint(offset);

        if (useTransformZAngle)
        {
            if (positionType == PositionType.Local && trans)
                angle = trans.eulerAngles.z;
            else if (positionType == PositionType.None && _sourceTrans)
                angle = _sourceTrans.eulerAngles.z;
        }
        Matrix4x4 angleMatrix = Matrix4x4.TRS(pos, Quaternion.Euler(0, 0, angle), Handles.matrix.lossyScale);
        using (new Handles.DrawingScope(angleMatrix))
        {
            //draw the objects
            if (detectType == DetectAreaType.Box)
                Handles.DrawWireCube(Vector2.zero, size);
            else if (detectType == DetectAreaType.Circle)
                Handles.DrawWireDisc(Vector2.zero, Vector3.back, radius);
        }

    }
#endif
}


