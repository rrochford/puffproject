﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer (typeof (InteractFXLoop))]
public class InteractFXLoopPropertyDrawer : PropertyDrawer
{
    private SerializedProperty sourceRef;

    private SerializedProperty interacts;
    private SerializedProperty delay;
    private SerializedProperty repeat;
    private SerializedProperty repeatDelay;
    private SerializedProperty totalTime;


    //need to set field amount manually if you add more fields
    private int fieldAmount = 5;
    private float fieldSize = 16;
    private float padding = 2;

    private bool expanded;

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        sourceRef = property;
        GetProperties();
        SetFieldAmount();
            //set the height of the drawer by the field size and padding
            return (fieldSize * fieldAmount) + (padding * fieldAmount);

    }

    public virtual void GetProperties()
    {
        //get property values
        interacts = sourceRef.FindPropertyRelative("interacts");
        delay = sourceRef.FindPropertyRelative("delay");
        repeat = sourceRef.FindPropertyRelative("repeat");
        repeatDelay = sourceRef.FindPropertyRelative("repeatDelay");
        totalTime = sourceRef.FindPropertyRelative("totalTime");
    }

    public virtual void SetFieldAmount()
    {
        fieldAmount = 5;
        if (interacts.isExpanded)
        {
            fieldAmount++;
            for (int i = 0; i < interacts.arraySize; i++)
            {
                fieldAmount++;
            }
        }
        if (repeat.boolValue)
        {
            fieldAmount++;
        }
    }

    // Draw the property inside the given rect
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        // Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.
        EditorGUI.BeginProperty(position, label, property);

        //divide all field heights by the field amount..then minus the padding
        position.height /= fieldAmount; position.height -= padding;

        // Draw Prefix label...this will push all other content to the right
        //position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        // Draw non-indented label instead
        EditorGUI.LabelField(position, property.displayName);

        // Get the start indent level
        var indent = EditorGUI.indentLevel;
        // Set indent amount
        EditorGUI.indentLevel = indent + 1;

        DisplayGUIElements(position, property, label);

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }

    public virtual void DisplayGUIElements(Rect position, SerializedProperty property, GUIContent label)
    {
        //offset position.y by field size
        position.y += fieldSize + padding;
        //first field
        EditorGUI.PropertyField(position, interacts, true);
        if (interacts.isExpanded)
        {
            //offset position.y by field size
            position.y += fieldSize + padding;
            for (int i = 0; i < interacts.arraySize; i++)
            {
                //offset position.y by field size
                position.y += fieldSize + padding;
            }
        }

        //detect type
        position.y += fieldSize + padding;
        EditorGUI.PropertyField(position, delay);

        //position type
        position.y += fieldSize + padding;
        EditorGUI.PropertyField(position, repeat);

        //Local position type
        if (repeat.boolValue)
        {
            position.y += fieldSize + padding;
            EditorGUI.PropertyField(position, repeatDelay);
        }
        //worldPos

            position.y += fieldSize + padding;
            EditorGUI.PropertyField(position, totalTime);

    }

}