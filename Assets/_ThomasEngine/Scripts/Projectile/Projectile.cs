﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Projectile : Linkable
{
    public new ProjectileData Data { get { return (ProjectileData)data; } }
    private float lifeTimer;

    protected float speed;
    public float Speed { get { return speed; } }
    protected int damage;
    public int Damage { get { return damage; } }
    protected LayerMask mask;
    public LayerMask Mask { get { return mask; } }
    protected Transform target;
    public Transform Target { get { return target; } }
    protected Vector2 targetPos;
    public Vector2 TargetPos { get { return targetPos; } }
    protected Vector2 direction;
    public Vector2 Direction { get { return direction; } }
    protected Transform sender;
    public Transform Sender { get { return sender; } }
    protected Vector2 lastPos;

    private Collider2D hit;
    private RaycastHit2D rayHit;
    private Vector2 hitPos;
    private Vector2 hitNormal;
    private int curhitAmount;

    protected bool bounced;
    protected bool paused;

    public virtual void Awake()
    {
        lastPos = transform.position;
    }

    // Update is called once per frame
    public virtual void Update()
    { 
        KillTimer();
    }

    public virtual void FixedUpdate()
    {
        DetectCollision();
    }

    public virtual void ShootProjectile(float _speed, int _damage, Vector2 _direction, LayerMask _mask,Transform _sender = null,Transform _target = null, Vector2 _targetPos = default(Vector2), bool _bounced = false)
    {
        speed = _speed;
        damage = _damage;
        mask = _mask;
        sender = _sender;
        target = _target;
        targetPos = _targetPos;
        direction = _direction;
        bounced = _bounced;
    }

    void DetectCollision()
    {
        switch (Data.detectType)
        {
            case ProjectileData.DetectType.Circle:
                DoCircleDetection();
                break;
            case ProjectileData.DetectType.Box:
                DoBoxDetection();
                break;
            case ProjectileData.DetectType.LineCast:
                DoLineDetection();
                break;
        }

        if (hit)
        {

            if (Data.spawnOnImpact.Length > 0)
            {
                DoSpawnOnImpact(hitPos, hitNormal);
            }

            if (Data.interactFX.Length > 0)
                DoInteractFX(hit.gameObject);

            Unit unit = hit.GetComponent<Unit>();
            if (unit)
            {
                DoDamage(unit);
            }

            curhitAmount++;
            if (curhitAmount >= Data.hitMaxAmount)
                KillProjectile();      
        }

        lastPos = transform.position;
    }

    void DoCircleDetection()
    {
        hit = Physics2D.OverlapCircle(transform.position, Data.detectRadius, mask);
        if (hit)
        {
            var ray = Physics2D.Raycast(transform.position, transform.right, Mathf.Infinity, mask);
            hitPos = ray.point;
            hitNormal = ray.normal;
        }
    }

    void DoBoxDetection()
    {
        hit = Physics2D.OverlapBox(transform.TransformPoint(Data.detectOffset), Data.detectSize, transform.eulerAngles.z, mask);
        if (hit)
        {
            var ray = Physics2D.Raycast(transform.position, transform.right, Mathf.Infinity, mask);
            hitPos = ray.point;
            hitNormal = ray.normal;
        }
    }

    void DoLineDetection()
    {
        rayHit = Physics2D.Linecast(lastPos, transform.position, mask);
        if (rayHit)
        {
            hitPos = rayHit.point;
            hitNormal = rayHit.normal;
            hit = rayHit.collider;
        }
    }

    void DoDamage(Unit _unit)
    {
        _unit.DamageHp(damage);
    }

    void DoSpawnOnImpact(Vector2 _pos, Vector2 _normal)
    {
        foreach (var spawn in Data.spawnOnImpact)
        {
            Instantiate(spawn, hitPos, Quaternion.LookRotation(Vector3.forward, hitNormal));
        }
    }

    void DoInteractFX(GameObject _reciever)
    {
        foreach (var interact in Data.interactFX)
        {
            interact.DoFX(this.gameObject, _reciever);
        }
    }

    void KillTimer()
    {
        lifeTimer += Time.deltaTime;
        if (lifeTimer > Data.lifeTime)
        {
            KillProjectile();
        }
    }

    public virtual void PauseProjectile(bool _pause)
    {
        paused = _pause;
    }

    public virtual void KillProjectile()
    {
        Destroy(this.gameObject);
    }
}
