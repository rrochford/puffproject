﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDraggable : ItemAimable
{
    public new ItemDraggableData Data { get { return (ItemDraggableData)data; } }

    private Collider2D col;
    private Coroutine curRoutine;
    private Rigidbody2D rb;
    private Vector2 dir;
    public override void Start()
    {
        base.Start();
    }

    public override void UseItem()
    {
        base.UseItem();
        curRoutine = StartCoroutine(StartDrag());
    }

    public override void StopUseItem()
    {
        base.StopUseItem();
        ThrowItem();
        if (curRoutine != null)
            StopCoroutine(curRoutine);
    }

    void ThrowItem()
    {
        if (!rb)
            return;

       rb.AddForce(dir * Data.throwForce, ForceMode2D.Impulse);
    }

    IEnumerator StartDrag()
    {
        col = Data.dragZone.DetectCollider(controller.AimPos);
        if (col)
        {
            rb = col.GetComponent<Rigidbody2D>();
            var startPos = col.transform.position;
            dir = controller.AimDirection;
            var lastPos = Vector2.zero;
            var nextPos = startPos;

            var rbDir = dir;

            while (col)
            {
                var aim = controller.AimPos;
                dir = aim - lastPos;
                if (Data.consistentThrowPower)
                    dir.Normalize();
                nextPos = new Vector3(aim.x, aim.y, startPos.z);
                var sens = Data.dragSensitivity;
                //calculate sensitivity based on weight
                if (Data.sensitivityByWeight)
                {
                    if (rb)
                    {
                        //get collider mass
                        var weight = rb.mass;
                        //get percentage or cur weight based on min and max weight
                        var perc = Mathf.InverseLerp(Data.maxWeight, Data.minWeight, weight);
                        //multiply drag sensitivity by this percentage
                        sens = Data.dragSensitivity * perc;
                    }      
                }
                if (Data.allowDragThroughColliders)
                {
                    col.transform.position = Vector3.Lerp(col.transform.position, nextPos, sens * Time.deltaTime);
                }
                else
                {
                    rbDir = (controller.AimPos - rb.position);
                    rb.velocity = Vector2.Lerp(Vector2.zero, rbDir, sens * Time.deltaTime) * (sens * 5);
                }
                
                //get last pos
                lastPos = aim;
                yield return new WaitForFixedUpdate();
            }
                
        }
        
    }

}
