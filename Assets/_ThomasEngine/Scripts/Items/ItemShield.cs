﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemShield : ItemAimable
{
    [System.Serializable]
    private class HeldCol
    {
        public Collider2D col;
        public Vector2 localPos;
        public Vector2 dir;
        public float dist;

        public bool IsMatch(Collider2D _col)
        {
            if (col)
                return col.transform == _col.transform;
            else
                return false;
        }
    }

    public new ItemShieldData Data { get { return (ItemShieldData)data; } }
    public GameObject spawnedShield;

    private bool shielding;

    private Collider2D[] cols;
    private float lastLength;

    private List<HeldCol> heldCols = new List<HeldCol>();
    private bool holding;
    public int lastHeldColsCount;

    private bool rotLocked;
    private Quaternion lastRot;
    private Vector2 aimPos;

    public override void Start()
    {
        base.Start();
    }

    public override void UseItem()
    {
        shielding = !shielding;
        if (shielding)
        {
            if (!spawnedShield)
                spawnedShield = Instantiate(Data.shieldPrefab, muzzle.position, muzzle.rotation);
            StartCoroutine(StartShielding());
        }
        else
            Destroy(spawnedShield.gameObject);
    }

    public override void OnDisable()
    {
        base.OnDisable();

        shielding = false;
        if (spawnedShield)
            Destroy(spawnedShield.gameObject);
    }

    private void Update()
    {
        GetInputs();
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        DetectHolding();

    }
    void GetInputs()
    {
        holding = Input.GetButton(Data.grabButton.stringValue);
    }

    IEnumerator StartShielding()
    {
        rotLocked = false;
        lastRot = new Quaternion();
        EnableUnitMovement(false);
        while (shielding)
        {
            CheckDeactivate();
            RotateShield();
            CheckHeightLock();
            ClampShieldDistance();
            SetShieldPosition();

            if (Data.bounceProjectiles && !overheated && !reloading)
            {
                DetectBounce();
            }

            yield return new WaitForFixedUpdate();
        }
        EnableUnitMovement(true);
    }

    void EnableUnitMovement(bool _enable)
    {
        if (!Data.disableUnitMovementWhenShieldActive)
            return;

        var unitCont = curUnitOwner.GetComponent<UnitController>();
        if (unitCont)
        {
            unitCont.DisableMovement(!_enable);
            unitCont.JumpEnabled = _enable;
        }
    }

    void CheckDeactivate()
    {
        if (Data.deactivateShieldOnReload)
        {
            if (reloading && spawnedShield.activeSelf)
            {
                EnableUnitMovement(reloading);
                spawnedShield.SetActive(!reloading);
            }  
            else if (!spawnedShield.activeSelf)
            {
                EnableUnitMovement(reloading);
                spawnedShield.SetActive(!reloading);
            }
                
        }
    }

    void RotateShield()
    {
        //face shield direction
        spawnedShield.transform.up = controller.AimDirection;
    }

    void CheckHeightLock()
    {
        aimPos = controller.AimPos;
        if (Data.lockToModelPrefabHeight)
        {
            if (controller.AimPos.y < muzzle.position.y)
            {
                if (!rotLocked)
                {
                    lastRot = spawnedShield.transform.rotation;
                    rotLocked = true;
                }

                aimPos = new Vector2(aimPos.x, muzzle.position.y);
                spawnedShield.transform.rotation = lastRot;
            }
            else
                rotLocked = false;
        }
    }

    void ClampShieldDistance()
    {
        var dist = Vector2.Distance(muzzle.position, aimPos);
        //set clamp distance
        var curclamp = Mathf.Clamp(dist, Data.minDistance, Data.maxDistance);
        //set shield pos
        Vector2 origin = aimPos - (Vector2)muzzle.position;
        origin *= curclamp / dist;
        aimPos = (Vector2)muzzle.position + origin;
    }

    void SetShieldPosition()
    {
        spawnedShield.transform.position = aimPos;
    }

    void DetectBounce()
    {
        cols = Physics2D.OverlapBoxAll(spawnedShield.transform.TransformPoint(Data.detectOffset), Data.detectSize, spawnedShield.transform.eulerAngles.z, Data.bounceMask);

        if (cols.Length != lastLength)
        {
            if (lastLength < cols.Length)//entered
            {
                if (holding)
                {
                    heldCols.Clear();
                    foreach (var col in cols)
                        AddHeldCol(col);
                    if (lastHeldColsCount != heldCols.Count)
                    {
                        RemoveAmount(1);
                        lastHeldColsCount = heldCols.Count;
                    }
                }
                else
                    BounceObject(cols[0]);

            }

            if (lastLength > cols.Length)//exited
            {
            }
            lastLength = cols.Length;
        }

    }

    void DetectHolding()
    {
        if (Data.grabObjects)
        {
            if (holding && !reloading)
            {
                for (int i = 0; i < heldCols.Count; i++)
                {
                    //set shield pos
                    var worldPos = spawnedShield.transform.TransformPoint(heldCols[i].localPos);
                    if (heldCols[i].col)
                        heldCols[i].col.transform.position = worldPos;
                }
            }
            else if (heldCols.Count > 0)
            {
                foreach (var heldCol in heldCols)
                {
                    if (heldCol.col)
                    {
                        if (Data.deactivateShieldOnReload && reloading)
                            Destroy(heldCol.col.gameObject);
                        else
                            BounceObject(heldCol.col);
                    }

                }
                heldCols.Clear();
            }

        }
    }

    void AddHeldCol(Collider2D _col)
    {
        var pro = _col.GetComponent<Projectile>();
        if (pro)
            pro.PauseProjectile(true);
        Vector2 pos = _col.transform.position;
        var heldCol = new HeldCol()
        {
            col = _col,
            localPos = spawnedShield.transform.InverseTransformPoint(pos),
            dir = (pos - (Vector2)spawnedShield.transform.position).normalized,
            dist = Vector2.Distance(spawnedShield.transform.position, pos)
        };
        heldCols.Add(heldCol);
    }

    void BounceObject(Collider2D _col)
    {
        //did bullet hit back of shield?
        var dir = (_col.transform.position - spawnedShield.transform.position).normalized;
        var localDir = spawnedShield.transform.InverseTransformDirection(dir);
        if (localDir.y < 0)
            return;

        var pro = _col.GetComponent<Projectile>();
        if (!pro)
        {
            Utils.LogComponentNullError(typeof(Projectile), _col.gameObject);
            return;
        }

        var force = Data.bounceForce;
        if (Data.useIncomingForce)
            force = pro.Speed;
        var mask = Data.damageMask;
        if (!Data.changeDamageMaskAfterBounce)
            mask = pro.Mask;
        pro.PauseProjectile(false);
        pro.ShootProjectile(force, pro.Damage, controller.AimDirection, mask, pro.Sender, pro.Target, pro.TargetPos, true);
        RemoveAmount(1);

    }
}
