﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemForcePush : ItemAimable
{
    public new ItemForcePushData Data { get { return (ItemForcePushData)data; } }
    private string pushButton;
    private string pullButton;
    private Collider2D curCol;
    private Collider2D[] cols;
    private ContactFilter2D con;
    public override void Start()
    {
        base.Start();
        SetupContactFilter();
        pushButton = Data.pushProperty.button.stringValue;
        pullButton = Data.pullProperty.button.stringValue;
    }

    void Update()
    {
        GetInputs();
    }

    void SetupContactFilter()
    {
        con = new ContactFilter2D
        {
            useLayerMask = true,
            layerMask = Data.mask
        };
    }

    void GetInputs()
    {
        if (Input.GetButtonDown(pushButton))
            StartCoroutine(StartPhysics(Data.pushProperty,controller.AimDirection, Data.pushProperty.force, Data.pullProperty.forceArea));
        if (Input.GetButtonDown(pullButton))
            StartCoroutine(StartPhysics(Data.pullProperty, -controller.AimDirection, Data.pullProperty.force, Data.pullProperty.forceArea));

    }

    IEnumerator StartPhysics(ItemForcePushData.PhysicsProperty _property, Vector2 _dir, float _force, Collider2D _col)
    {
        muzzle.right = controller.AimDirection;
        if (!curCol)
            curCol = Instantiate(_col, muzzle.position, muzzle.rotation);
        DoPhysics(_property, _dir, _force);
        yield return new WaitForEndOfFrame();
        if (_property.allowHolding)
        {
            while (Input.GetButton(_property.button.stringValue))
            {
                DoPhysics(_property, _dir, _force);
                yield return new WaitForFixedUpdate();
            }
            
        }
        Destroy(curCol.gameObject);
    }

    void DoPhysics(ItemForcePushData.PhysicsProperty _property, Vector2 _dir, float _force)
    { 
        curCol.transform.right = controller.AimDirection;
        curCol.transform.position = muzzle.position;
        cols = new Collider2D[Data.maxObjects];
        Physics2D.OverlapCollider(curCol, con, cols);
        for (int i = 0; i < cols.Length; i++)
        {
            if (cols[i] != null)
            {
                var rb = cols[i].GetComponent<Rigidbody2D>();
                if (rb)
                {
                    if (_property.consistentForce)
                        rb.Sleep();
                    rb.AddForce(_dir * _force, ForceMode2D.Impulse);
                }
                    
            }

        }     

    }

}
