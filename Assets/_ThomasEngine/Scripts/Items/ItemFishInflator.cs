﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemFishInflator : ItemUseable
{
    public new ItemFishInflatorData Data { get { return (ItemFishInflatorData)data; } }
    [SerializeField] private DetectZone detectZone;

    private FishController fishController;
    private Coroutine itemCoroutine;

    private float lastHP;
    public override void InitializeItem()
    {
        base.InitializeItem();
        fishController = curUnitOwner.GetComponent<FishController>();
        lastHP = curUnitOwner.CurHP;
    }

    public override void UseItem()
    {
        if (empty || overheated || reloading)
            return;
        if (itemCoroutine != null)
            StopCoroutine(itemCoroutine);
        itemCoroutine = StartCoroutine(StartInflate());
        fishController.Inflate(true);
    }

    public override void StopUseItem()
    {
        if (itemCoroutine != null)
            StopCoroutine(itemCoroutine);
    }

    IEnumerator StartInflate()
    {
        lastHP = curUnitOwner.CurHP;
        while (!empty && !overheated && !reloading)
        {
            yield return new WaitForEndOfFrame();
            if (curUnitOwner.CurHP < lastHP)
            {
                RemoveAmount(totalAmount);
                lastHP = curUnitOwner.CurHP;
            }    
            else
                RemoveAmount(Data.amountDrainSpeed * Time.deltaTime);
        }
        fishController.Inflate(false);
    }

}
