﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEquip : UnitEquip
{

    [SerializeField] private InputProperty[] quickMenuButtons;
    [SerializeField] private bool seperateUseButtonsForItems;
    [SerializeField] private InputProperty[] useButtons;
    [SerializeField] private InputProperty useButton;
    [SerializeField] private bool enableToggleSwitch;
    [SerializeField] private InputProperty toggleForwardsButton;
    [SerializeField] private InputProperty toggleBackwardsButton;
    [SerializeField] private InputProperty equipButton;
    [SerializeField] private InputProperty dropButton;

    //private UserDataManager userData;
    private QuickMenuUI quickMenu;
    private Player player;


    private void Update()
    {
        GetInputs();
        CheckSpawnLocation();
    }

    public override void GetComponents()
    {
        base.GetComponents();
        player = (Player)unit;
        //GameManager gm = GameManager.instance;
        //userData = gm.GetUserDataManager();
        if (player.UI)
            quickMenu = player.UI.QuickMenu;
    }

    void CheckSpawnLocation()
    {
        //reload weapons if spawnlocation is destroyed...usually a skin swap
        if (!spawnLoc)
        {
            GetSpawnLocation();
            SpawnItems();
        }

    }

    public override void SpawnItems()
    {
        base.SpawnItems();
        if (quickMenu)
            quickMenu.UpdateQuickMenuItems(itemDatas);
    }

    void GetInputs()
    {
        //loop through all quick commands
        if (curItems != null)
        {
            if (seperateUseButtonsForItems)
            {
                for (int i = 0; i < useButtons.Length; i++)
                {
                    if (Input.GetButtonDown(useButtons[i].stringValue))
                    {
                        SetCurItem(i);
                        curItem.UseItem();
                    }
                    else if (Input.GetButtonUp(useButtons[i].stringValue))
                    {
                        SetCurItem(i);
                        curItem.StopUseItem();
                    }
                        
                }
            }
            else if (!setAllItemsActive)
            {
                for (int i = 0; i < curItems.Length; i++)
                {
                    if (Input.GetButtonDown(quickMenuButtons[i].stringValue))
                    {
                        SetCurItem(i);
                    }
                }

                if (enableToggleSwitch)
                {
                    if (Input.GetButtonDown(toggleForwardsButton.stringValue))
                        SwitchToNextItemForward();
                    if (Input.GetButtonDown(toggleBackwardsButton.stringValue))
                        SwitchToNextItemBackward();
                }
            }
            
        }
        
        if (Input.GetButtonDown(equipButton.stringValue) && !autoEquipItems)
        {
            EquipCurItem(!equipped);
        }

        if (equipped && !seperateUseButtonsForItems)
        {
            if (Input.GetButtonDown(useButton.stringValue))
                curItem.UseItem();
            else if (Input.GetButtonUp(useButton.stringValue))
                curItem.StopUseItem();
            if (Input.GetButtonDown(dropButton.stringValue))
                    DropCurrentItem();

        }
        
    }

    public override void AddItem(Item _item)
    {
        base.AddItem(_item);
        if (quickMenu)
            quickMenu.UpdateQuickMenuItems(itemDatas);
    }

    public override void RemoveCurrentItem()
    {
        base.RemoveCurrentItem();
        //update ui
        if (quickMenu)
            quickMenu.EquipToSlot(null, curInd);
    }

    public override void RemoveItem(int _ind)
    {
        base.RemoveItem(_ind);
        //update ui
        if (quickMenu)
            quickMenu.EquipToSlot(null, _ind);
    }

    public override void SetCurItem(int _itemInd)
    {
        base.SetCurItem(_itemInd);
        //update ui
        if (quickMenu)
            quickMenu.SetActiveSlot(_itemInd);
    }

    protected override void SwitchActiveItem(int _itemInd)
    {
        base.SwitchActiveItem(_itemInd);
        //ui
        if (quickMenu)
            quickMenu.SetActiveSlot(_itemInd);

    }

}
