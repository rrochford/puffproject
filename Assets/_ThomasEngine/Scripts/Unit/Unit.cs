﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
    [SerializeField] protected UnitData data;
    [SerializeField] protected ChildName itemSpawnLocation;
    [SerializeField] protected GameObject skinOverride;
    protected Transform itemSpawnTrans;
    public Transform ItemSpawnLocation { get { return itemSpawnTrans; } }
    [SerializeField] private Behaviour[] disableOnDeath;

    protected UnitData curData;
    public UnitData CurUnitData { get { return curData; } }
    [SerializeField] protected Transform attackTarget;
    public Transform AttackTarget { get { return attackTarget; } }

    protected int curMaxHP = 5;
    public int MaxHp { get { return curMaxHP; } set { curMaxHP = value; } }
    protected int curLives;
    public int CurLives { get { return curLives; } }
    protected FloatWrapper curHP = new FloatWrapper(0);
    public float CurHP { get { return curHP.Value; } }

    protected bool stunned;
    public bool IsStunned { get { return stunned; } }
    private Coroutine stunnedCoroutine;

    protected bool physicsIgnored;
    public bool IsPhysicsIgnored { get { return physicsIgnored; } }
    private Coroutine physicsIgnoredCoroutine;

    protected bool meshesChanged;
    public bool IsMeshesChanged { get { return meshesChanged; } }
    private Renderer[] meshesToChange;
    private Material[] startMeshesMaterials;
    private Coroutine meshChangeCoroutine;

    protected bool invincible;
    public bool IsInvincible { get { return invincible; } set { invincible = value; } }
    private Coroutine invincibleCoroutine;

    protected float curWeight;
    public float CurWeight { get { return curWeight; } }
    protected bool dead;
    public bool IsDead { get { return dead; } }
    protected UnitController controller;
    protected UIUnit ui;
    public UIUnit UI { get { return ui; } }

    protected List<UnitBuff> curBuffs = new List<UnitBuff>();

    protected UnitAnimations anim;
    protected Rigidbody2D rb;
    protected Collider2D col;
    public Collider2D Collider { get { return col; } }

    protected Vector2 spawnPos;
    protected GameObject curSkin;

    protected virtual void Awake()
    {
        GetComponents();
        spawnPos = transform.position;
        if (!data.setSkin)
            itemSpawnTrans = itemSpawnLocation.transformValue;
        SetFirstData();
    }

    protected virtual void Update()
    {
        CheckKillHeight();
    }

    protected virtual void GetComponents()
    {
        controller = GetComponent<UnitController>();
        anim = GetComponent<UnitAnimations>();
        rb = GetComponent<Rigidbody2D>();
        col = GetComponent<Collider2D>();
    }

    protected virtual void SetFirstData()
    {
        SetData(data, true);
    }

    public virtual void SetData(UnitData _data, bool _resetLives = true)
    {
        curData = _data;
        //update unit stats

        //weight
        curWeight = curData.weight;
        if (rb)
            rb.mass = curWeight;

        //set collider size
        var capsule = col as CapsuleCollider2D;
        if (capsule)
        {
            capsule.size = curData.skinSize;
            capsule.offset = new Vector2(0, capsule.size.y / 2);
        }

        //update controller stats
        if (controller)
        {
            controller.BaseSpeed = curData.speed;
            controller.JumpPower = curData.jumpPower;
            controller.StartColSize = curData.skinSize;
            controller.StartColOffset = curData.skinSize / 2;
        }

        //spawn ui
        if (ui)
            Destroy(ui.gameObject);
        if (curData.spawnUI)
        {
            if (curData.UIToSpawn)
            {
                ui = Instantiate(curData.UIToSpawn);
            }
        }

        if (curData.setSkin)
        {
            //destroy current skin
            if (curSkin)
                Destroy(curSkin);

            //spawn new skin
            curSkin = Instantiate(curData.skinPrefab, transform.position, transform.rotation);
            curSkin.transform.SetParent(transform);
            //set Rotation
            curSkin.transform.localEulerAngles = new Vector2(transform.rotation.x, transform.rotation.y + curData.skinRotation);
            itemSpawnLocation = curData.itemSpawnLocation;
        }
        else if (skinOverride)
            curSkin = skinOverride;

        if (curSkin)
        {
            //set animator
            var an = curSkin.GetComponent<Animator>();
            if (anim)
                anim.Animator = an;

            itemSpawnTrans = curSkin.transform.FindDeepChild(itemSpawnLocation.stringValue);

            GetMaterials();
        }
        else
            Debug.Log("No skin is set up on " + gameObject.name + "! You must assign a Skin Override on " + this + " or set skin on " + curData);

        SetupUnit(_resetLives);
    }

    protected virtual void SetupUnit(bool _resetLives)
    {
        dead = false;
        //health
        curMaxHP = curData.maxHP;
        curHP.Value = curMaxHP;

        //lives
        if (_resetLives)
            curLives = curData.maxLives;

        //buffs
        ActivateAllBuffs(false);
        curBuffs.Clear();
        if (curData.buffs.Length > 0)
            curBuffs.AddRange(curData.buffs);
        ActivateAllBuffs(true);

        //behaviours
        ActivateDeathBehaviours(true);

        if (ui)
            RefreshUIValues();
    }

    public virtual void AddBuff(UnitBuff _buffToAdd)
    {
        _buffToAdd.ActivateBuff(this, true);
        curBuffs.Add(_buffToAdd);
    }

    public virtual void RemoveBuff(UnitBuff _buffToRemove)
    {
        if (!curBuffs.Contains(_buffToRemove))
            return;

        _buffToRemove.ActivateBuff(this, false);
        curBuffs.Remove(_buffToRemove);
    }

    public virtual void ActivateAllBuffs(bool _activate)
    {
        foreach (var buff in curBuffs)
        {
            buff.ActivateBuff(this, _activate);
        }
    }

    public virtual void AddHp(float _amount)
    {
        //only add health if not at max
        if (curHP.Value < curMaxHP)
        {
            curHP.Value += _amount;
        }

    }

    protected virtual void CheckKillHeight()
    {
        if (dead)
            return;

        if (transform.position.y < GameManager.instance.GetKillHeight())
        {
            DamageHp(curHP.Value);
        }
    }

    public virtual void DamageHp(float _damage)
    {
        if (dead || invincible)
            return;

        curHP.Value -= _damage;
        curHP.Value = Mathf.Clamp(curHP.Value, 0, Mathf.Infinity);
        if (ui)
            RefreshUIValues();

        //anim
        if (anim)
            anim.PlayHurt();

        Invincible();
        Stun();
        IgnorePhysics();
        ChangeMeshes();

        //kill player if health at 0
        if (curHP.Value <= 0)
        {
            curHP.Value = 0;
            Die();
        }

    }

    void Invincible()
    {
        if (!curData.invincibleOnHit)
            return;

        if (invincibleCoroutine != null)
            StopCoroutine(invincibleCoroutine);
        invincibleCoroutine = StartCoroutine(StartInvincible());
    }

    IEnumerator StartInvincible()
    {
        invincible = true;
        yield return new WaitForSeconds(curData.invincibleTime);
        invincible = false;
    }

    void Stun()
    {
        if (!curData.stunOnHit)
            return;

        if (stunnedCoroutine != null)
            StopCoroutine(stunnedCoroutine);
        stunnedCoroutine = StartCoroutine(StartStun());
    }

    IEnumerator StartStun()
    {
        stunned = true;
        //anim sync
        if (anim)
            anim.PlayStunned();
        controller.DisableMovement(true);
        yield return new WaitForSeconds(curData.stunTime);
        controller.DisableMovement(false);
        stunned = false;
        //anim sync
        anim.PlayIdle();
    }

    void IgnorePhysics()
    {
        if (!curData.ignorePhysicsOnHit)
            return;

        if (physicsIgnoredCoroutine != null)
            StopCoroutine(physicsIgnoredCoroutine);
        physicsIgnoredCoroutine = StartCoroutine(StartIgnorePhysicsLayers());
    }

    IEnumerator StartIgnorePhysicsLayers()
    {
        physicsIgnored = true;
        IgnoreLayers(true);
        yield return new WaitForSeconds(curData.ignoreTime);
        IgnoreLayers(false);
        physicsIgnored = false;
    }

    void IgnoreLayers(bool _ignore)
    {
        foreach (var lay in curData.ignoreLayers)
        {
            Physics2D.IgnoreLayerCollision(gameObject.layer, lay.indexValue, _ignore);
        }
    }

    void ChangeMeshes()
    {
        if (!curData.changeMeshMaterialOnHit)
            return;

        if (meshChangeCoroutine != null)
            StopCoroutine(meshChangeCoroutine);
        meshChangeCoroutine = StartCoroutine(StartMeshChange());
    }

    void GetMaterials()
    {
        if (!curData.changeMeshMaterialOnHit)
            return;

        meshesToChange = curSkin.GetComponentsInChildren<Renderer>();
        if (meshesToChange.Length > 0)
        {
            startMeshesMaterials = new Material[meshesToChange.Length];
            for (int i = 0; i < meshesToChange.Length; i++)
            {
                startMeshesMaterials[i] = meshesToChange[i].material;
            }
        }
        else
            Debug.Log("No Meshrenderers found on " + curSkin);

    }

    IEnumerator StartMeshChange()
    {
        ActivateChangeMesh(true);
        yield return new WaitForSeconds(curData.changeMeshTime);
        ActivateChangeMesh(false);

    }

    void ActivateChangeMesh(bool _activate)
    {
        if (_activate)
        {
            for (int i = 0; i < meshesToChange.Length; i++)
            {
                if (meshesToChange[i])
                    meshesToChange[i].material = curData.materialToUse;
            }
        }
        else
        {
            for (int i = 0; i < meshesToChange.Length; i++)
            {
                if (meshesToChange[i])
                    meshesToChange[i].material = startMeshesMaterials[i];
            }
        }

    }

    public virtual void RefreshUIValues()
    {
        ui.SetHPValue(curHP.Value);
    }

    public virtual void DamageHp(float _damage, float _bounceForce, Vector2 _bounceDir)
    {
        if (dead)
            return;

        DamageHp(_damage);
        if (controller)
            controller.Bounce(_bounceDir, _bounceForce);

    }

    public virtual void Die(string _reason = default(string))
    {
        dead = true;
        curLives--;
        ActivateAllBuffs(false);

        //anim
        if (anim)
            anim.PlayDead();

        DoDeathFX();
        DoNoLivesOptions();
        StartCoroutine(StartDeath());
    }

    void DoDeathFX()
    {
        if (curData.deathFX.Length > 0)
        {
            foreach (var fx in curData.deathFX)
            {
                fx.DoFX(gameObject, gameObject);
            }
        }
    }

    void DoNoLivesOptions()
    {
        if (curData.noLivesOptions == UnitData.NoLivesType.GameOverWin)
            GameManager.instance.LevelWin(gameObject.name + " has died!", curData.endTime, true);
        else if (curData.noLivesOptions == UnitData.NoLivesType.GameOverLose)
        {
            if (curLives <= 0)
                GameManager.instance.GameOverLose();
        }
            
    }

    IEnumerator StartDeath()
    {
        ActivateDeathBehaviours(false);

        Vector2 lastPos = transform.position;
        float timer = 0;
        while (timer < curData.deathTime)
        {
            timer += Time.deltaTime;
            if (timer > curData.deathTime)
                timer = curData.deathTime;
            if (curData.deathMovement == UnitData.DeathMovement.StopIfGrounded)
            {
                if (controller.IsGrounded)
                    transform.position = lastPos;
                else
                    lastPos = transform.position;
            }
            else if (curData.deathMovement == UnitData.DeathMovement.FallThrough)
            {
                if (col)
                    col.enabled = false;
            }   
            yield return new WaitForFixedUpdate();
        }

        if (curData.deathOptions == UnitData.DeathType.Respawn)
            Respawn();
        else if (curData.deathOptions == UnitData.DeathType.Destroy)
            Destroy(gameObject, curData.delay);
    }

    void ActivateDeathBehaviours(bool _activate)
    {
        //optional disable other components
        foreach (var deathComp in disableOnDeath)
        {
            if (deathComp)
                deathComp.enabled = _activate;
        }
    }

    protected virtual void Respawn()
    {
        if (curLives <= 0 && !curData.infiniteLives)
            return;

        StartCoroutine(StartRespawn());
    }

    public IEnumerator StartRespawn()
    {
        yield return new WaitForSeconds(curData.respawnTime);

        if (curData.resetLevelOnDeath)
            //reset level at checkpoint 
            ResetLevel();
        else
        {
            ResetUnitPosition(spawnPos);
            //reset player health
            SetupUnit(false);
        }
            
    }

    protected virtual void ResetUnitPosition(Vector2 _pos)
    {
        Debug.Log("resetting " + gameObject.name + " position to " + _pos);
        transform.position = _pos;
        if (rb)
            rb.Sleep();
        if (col)
            col.enabled = true;
    }

    void ResetLevel()
    {
        GameManager.instance.GetSceneTransitionData().ResetCurLevel();
    }

}
