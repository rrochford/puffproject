﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UnitDetect : MonoBehaviour
{
    [SerializeField] protected DetectZone detectZone;

    protected Collider2D col;
    protected bool detected;
    public bool IsDetected { get { return detected; } }
    private bool lastDetect;

    public virtual void OnEnter()
    {
        //do stuff
    }

    public virtual void OnStay()
    {
        //do stuff
    }

    public virtual void OnExit()
    {
        //do stuff
    }

    public virtual void FixedUpdate()
    {
        DoDetection();
    }

    void DoDetection()
    {
        col = detectZone.DetectCollider(transform);
        detected = col;

        if (lastDetect != detected)
        {
            if (detected)
                OnEnter();
            else
                OnExit();
        }
        else if (detected)
            OnStay();

        lastDetect = detected;
    }

}
