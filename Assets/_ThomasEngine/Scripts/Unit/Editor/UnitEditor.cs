﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Unit))]
public class UnitEditor : Editor
{
    protected Unit source;
    protected SerializedObject sourceRef;
    //skins
    protected SerializedProperty data;
    protected SerializedProperty skinOverride;
    protected SerializedProperty itemSpawnLocation;
    protected SerializedProperty disableOnDeath;
    //enemy configs
    protected SerializedProperty attackTarget;


    public virtual void OnEnable()
    {
        sourceRef = serializedObject;
        source = (Unit)target;
        GetProperties();
    }

    public override void OnInspectorGUI()
    {

        SetProperties();

        sourceRef.ApplyModifiedProperties();
    }

    public virtual void GetProperties()
    {
        //enemy
        attackTarget = sourceRef.FindProperty("attackTarget");
        //skins
        data = sourceRef.FindProperty("data");
        skinOverride = sourceRef.FindProperty("skinOverride");
        itemSpawnLocation = sourceRef.FindProperty("itemSpawnLocation");
        disableOnDeath = sourceRef.FindProperty("disableOnDeath");
    }

    protected virtual void SetProperties()
    {
        EditorGUILayout.Space();
        DisplayDataProperties();
        //enemy
        EditorGUILayout.PropertyField(disableOnDeath, true);
        EditorGUILayout.PropertyField(attackTarget);

    }

    protected virtual void DisplayDataProperties()
    {
        //skins
        EditorGUILayout.PropertyField(data);
        if (data.objectReferenceValue)
        {
            var unitData = data.GetRootValue<UnitData>();
            if (unitData)
            {
                if (!unitData.setSkin)
                {
                    EditorGUILayout.PropertyField(skinOverride);
                    itemSpawnLocation.ChildNamePopUpParentOverride(sourceRef, skinOverride);
                }
            }  
        }
        else
            EditorGUILayout.LabelField("You must assign a unit data file to " + data.displayName);
    }

}
