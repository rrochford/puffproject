﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lineRender : MonoBehaviour {

    // Place both objects to be connected by line rendering.
    public GameObject rock;
    public GameObject enemy;

	// Use this for initialization of line renderer from object to object.
	void Update () {

        LineRenderer lineRenderer = GetComponent<LineRenderer>();

        lineRenderer.SetPosition (0, rock.transform.position);
        lineRenderer.SetPosition (1, enemy.transform.position); 

	}
    
}
