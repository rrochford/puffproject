﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class inflateBubbles : MonoBehaviour
{

    public GameObject bigBubbles;
    public GameObject bubbles;

    public float repeatTime = 1;
    private float repeatTimer;

    private void Update()
    {    
        
        if (Input.GetButtonUp("Jump"))
        {
            Instantiate(bigBubbles, transform.position, bigBubbles.transform.rotation);

            

            repeatTimer = 0;
        }

        if (Input.GetButton("Jump"))
        {

            

            repeatTimer = 0;

            return;
         
        }

        repeatTimer += Time.deltaTime;

        if (repeatTimer>repeatTime)
        {
            Instantiate(bubbles, transform.position, bubbles.transform.rotation);
            

            //Reset timer after routine.
            repeatTimer = 0;
        }

    }
    

}
