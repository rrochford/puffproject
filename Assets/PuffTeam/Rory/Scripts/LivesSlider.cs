﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LivesSlider : MonoBehaviour {


    Animator ui_Animator;

    private int lives;
    private bool playing;

    // Use this for initialization
    private void Start()
    {
        //fetch animator for lives
        ui_Animator = GetComponent<Animator>();

        //update Lives Int in animator
        lives = GameManager.instance.SpawnedPlayer.CurLives;
        ui_Animator.SetInteger("Lives", lives);

        ui_Animator.Play("LivesSlideIn");
    }

    private void Update()
    {
        if (GameManager.instance.SpawnedPlayer.IsDead)
        {
            if (!playing)
            {
                ui_Animator.Play("LivesSlideIn");
                playing = true;
            }
        }

        else if (playing)
        {
            playing = false;
        }
    }
}
