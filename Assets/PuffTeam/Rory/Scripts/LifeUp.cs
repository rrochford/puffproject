﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HutongGames.PlayMaker;
using UnityEngine.UI;

public class LifeUp : MonoBehaviour {

    Animator ui_Animator;

    private int totalCoins;
    private int lives;

    private bool playing;

    public int coinsOneUp;

    void Start()
    {
        //Get lives.
        lives = GameManager.instance.SpawnedPlayer.CurLives;
        
        //Get slide animation for lives
        ui_Animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (totalCoins == coinsOneUp)
        {
            // Add one life.
            GameManager.instance.SpawnedPlayer.GetComponent<Player>().AddLives(1);

            Debug.Log("1UP");

            //Reset FSM coin int to 0.
            FsmVariables.GlobalVariables.GetFsmInt("NewCoinCount").Value = 0;

            if (!playing)
            {
                ui_Animator.Play("LivesSlideIn");
                playing = true;
            }            
        }

        else if (playing)
        {
            playing = false;
        }

        totalCoins = FsmVariables.GlobalVariables.GetFsmInt("NewCoinCount").Value;

        //Update total lives to go up one if total coins match the set amount.
                      
    }
}